# 関数定義の備忘録


## オプション引数が定義された関数
渡しても渡さなくても良い引数が定義された関数  
オプション引数が渡されなかった場合その仮引数は `undefined` となる  
`(オプション変数名?: 型): 戻り値の型 => { 処理 }` で定義する
```ts
const helloOpt = (opt?: string): string => !opt ? 'hello!' : `hello ${opt}!`;

console.log(helloOpt()); // hello!
console.log(helloOpt('option')); // hello option!
```


## デフォルト引数が定義された関数
[デフォルト引数 - JavaScript | MDN](https://developer.mozilla.org/ja/docs/Web/JavaScript/Reference/Functions/Default_parameters)  
渡されなかった場合や `undefined` が渡された場合に、予め決められた値になる引数を持った関数。  
`(デフォルト引数名 = 値): 戻り値の型 => { 処理 }` で定義する。  
通常の引数と合わせてデフォルト引数を定義するときは、**他のデフォルト引数ではない引数より後ろに定義**する。  
`(引数名: 型, デフォルト引数名 = 値): 戻り値の型 => { 処理 }`
```ts
const helloDefault = (defarg = 'world'): string => `hello ${defarg}!`;

console.log(helloDefault()); // hello world!
console.log(helloDefault('default')); // hello default!
```


## 残余引数が定義された関数
[残余引数 - JavaScript | MDN](https://developer.mozilla.org/ja/docs/Web/JavaScript/Reference/Functions/rest_parameters)  
1つ以上の不定の数の引数を定義するために使用する。 可変長引数とも。  
個々の引数で渡すことも、配列やタプルなどをスプレッド構文で渡すこともできる。  
`(...引数名: 型[]): 戻り値の型 => { 処理 }` で定義する。  
```ts
const sum = (...nums: number[]): number => {
  return nums.reduce((prev, crnt) => prev + crnt);
}

console.log(sum(1)); // 1
console.log(sum(7, 8, 9)); // 24

const array = [1, 2, 3, 4, 5];
console.log(sum(...array)); // 15
// sum(array) はエラー

const tuple: readonly [number, number] = [25, 75];
console.log(sum(...tuple)); // 100
// sum(tuple) はエラー
```


## コールバック関数を受け取る関数の定義
[Callback function (コールバック関数) - MDN Web Docs 用語集: ウェブ関連用語の定義 | MDN](https://developer.mozilla.org/ja/docs/Glossary/Callback_function)  
```ts
// 「引数の無いコールバック関数」を受け取り実行する関数を定義
const func1 = (callback: () => void): void => callback();
// 「文字列引数を受け取るコールバック関数」を受け取り実行する関数を定義
const func2 = (callback: (msg: string) => void): void => callback('func2');

func1(() => console.log('hello callback!')); // hello callback!
func2(console.log); // func2
```


## クロージャ
[クロージャ - JavaScript | MDN](https://developer.mozilla.org/ja/docs/Web/JavaScript/Closures)  
- エンクロージャ (外側の関数) を実行するとクロージャを返却する。
- クロージャはエンクロージャ内で定義された変数 (レキシカル変数) を利用できる。
- レキシカル変数はエンクロージャ内で定義されているのでグローバルな名前空間を占有しない。
- エンクロージャを実行するたびに新たなクロージャが作成される
```ts
// 実行するたびにカウントするクロージャを返却する
const enclosure = (): (() => number) => {
  let i = 1;
  return (): number => i++; // 関数を返却
};

const count = enclosure();
console.log(count()); // 1
console.log(count()); // 2
console.log(count()); // 3
```


## 宗教的な理由で三項演算子が使えない人のための即時実行関数
[IIFE (即時実行関数式) - MDN Web Docs 用語集: ウェブ関連用語の定義 | MDN](https://developer.mozilla.org/ja/docs/Glossary/IIFE)
```ts
const x = Math.floor(Math.random() * Math.floor(2)); // 0 か 1 の値を取得
const result = x === 1 ? 'hit!!' : 'miss...'; //三項演算子

console.log(result); // hit!! or miss
```
三項演算子が使えない場合は以下の様に即時実行関数で置き換えることができる
```ts
// 即時実行関数の基本的な定義方法
( ()=>{} )(); // 処理の無い無名関数をを実行

// 三項演算子は使わずに result に結果を格納
const result = (() => {
  if (Math.floor(Math.random() * Math.floor(2)) === 1) {
    return 'hit!!';
  }
  return 'miss...';
})();

console.log(result); // hit!! or miss...
```
