/**
 * コールバック関数を受け取って実行する
 * @argument { () => void } 引数・戻り値の無いコールバック
 * @returns { void }
 */
export const func1 = (callback: () => void): void => callback();

/**
 * 文字列を引数に取るコールバック関数を受け取って実行する
 * @argument { (msg: string) => void } 文字列を引数に取る返り値の無いコールバック
 * @returns { void }
 */
export const func2 = (cbk: (msg: string) => void): void => cbk('func2');
