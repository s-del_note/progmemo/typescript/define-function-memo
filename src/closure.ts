/**
 * クロージャを返却するエンクロージャ
 * @returns { () => number } カウントを進めるクロージャ
 */
export const enclosure = (): (() => number) => {
  /**
   * カウンタ変数 (レキシカル変数)
   * @var { number }
   */
  let i = 1;

  /**
   * カウントを進めるクロージャ
   * @return { function } 現在のカウンタの値を返してインクリメントする関数
   */
  return (): number => i++;
};
