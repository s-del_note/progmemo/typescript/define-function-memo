/**
 * オプション引数が定義された関数
 *
 * @param { string | undefined } 渡されなかった場合は undefined となる
 * @returns { string } 'hello!' か `hello ${opt}!` の文字列
 */
export const helloOpt = (opt?: string): string => {
  return !opt ? 'hello!' : `hello ${opt}!`;
};
